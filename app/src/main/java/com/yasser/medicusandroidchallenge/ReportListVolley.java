package com.yasser.medicusandroidchallenge;

import android.content.Context;
import android.util.Log;

import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

class ReportListVolley {

    private static final String GET_DATA = "https://retoolapi.dev/hZZ5j8/biomarkers";

    public static void getData(Context context, GetDataListener getDataListener) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(GET_DATA, response -> {
            ArrayList<ReportItemModel> items = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);
                    items.add(new ReportItemModel(
                            jsonObject.getInt("id"),
                            jsonObject.getString("date"),
                            jsonObject.getString("info"),
                            jsonObject.getString("color"),
                            jsonObject.getInt("value"),
                            jsonObject.getString("symbol"),
                            jsonObject.getString("insight"),
                            jsonObject.getString("category")
                    ));
                } catch (JSONException e) {
                    Log.e("response_parse", e.getMessage());
                }
            }

            Log.e("response", "" + items.size());
            getDataListener.onSuccess(items);

        }, error -> {
            Log.e("response_error", "" + error.getMessage());
            getDataListener.onError();
        });
        Volley.newRequestQueue(context).add(jsonArrayRequest);

    }

    public interface GetDataListener {
        void onSuccess(ArrayList<ReportItemModel> report);

        void onError();
    }

}
