package com.yasser.medicusandroidchallenge;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.reportsRV);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        progressDialog=new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("loading ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        ReportListVolley.getData(this, new ReportListVolley.GetDataListener() {
            @Override
            public void onSuccess(ArrayList<ReportItemModel> report) {
                progressDialog.dismiss();
                recyclerView.setAdapter(new AdapterReportList(report, MainActivity.this));
            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

}