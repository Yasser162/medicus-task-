package com.yasser.medicusandroidchallenge;

import java.io.Serializable;

class ReportItemModel implements Serializable {
    private int id;
    private String date;
    private String info;
    private String color;
    private int value;
    private String symbol;
    private String insight;
    private String category;

    public ReportItemModel(int id, String date, String info, String color, int value, String symbol, String insight, String category) {
        this.id = id;
        this.date = date;
        this.info = info;
        this.color = color;
        this.value = value;
        this.symbol = symbol;
        this.insight = insight;
        this.category = category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setInsight(String insight) {
        this.insight = insight;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getInfo() {
        return info;
    }

    public String getColor() {
        return color;
    }

    public int getValue() {
        return value;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getInsight() {
        return insight;
    }

    public String getCategory() {
        return category;
    }
}
