package com.yasser.medicusandroidchallenge;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class AdapterReportList extends RecyclerView.Adapter<AdapterReportList.ViewHolder> {
    ArrayList<ReportItemModel> reportForRecyclesList;
    Context context;

    public AdapterReportList(ArrayList<ReportItemModel> reportForRecyclesList, Context context) {
        this.reportForRecyclesList = reportForRecyclesList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(context)
                        .inflate(R.layout.resport_item_view, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ReportItemModel model = reportForRecyclesList.get(position);

        holder.symbol.setText(model.getSymbol());
        if (model.getColor().equals("#E64A19")) {
            holder.symbol.setBackgroundResource(R.drawable.text_design);
            holder.symbol.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            holder.symbol.setBackgroundResource(R.drawable.text_design2);
            holder.symbol.setTextColor(context.getResources().getColor(R.color.blue));
        }
        holder.date.setText(model.getDate());
        holder.value.setText("" + model.getValue());

        holder.itemView.setOnClickListener(v -> {
            context.startActivity(new Intent(context, ItemActivity.class)
                    .putExtra("item", model));
        });

    }

    @Override
    public int getItemCount() {
        return reportForRecyclesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView symbol;
        TextView date;
        TextView value;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            symbol = itemView.findViewById(R.id.symbol);
            date = itemView.findViewById(R.id.date);
            value = itemView.findViewById(R.id.value);
        }
    }
}
