package com.yasser.medicusandroidchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ItemActivity extends AppCompatActivity {

    private ReportItemModel reportItemModel;
    TextView category,symbol,value,insight,info,symbolWithColor,date,infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        category=findViewById(R.id.categoryItem);
        symbol=findViewById(R.id.symbolItem);
        date=findViewById(R.id.dateItem);
        symbolWithColor=findViewById(R.id.symbolItemWithColor);
        insight=findViewById(R.id.insightItem);
        info=findViewById(R.id.infoItem);
        infoText=findViewById(R.id.infoText);
        value=findViewById(R.id.valueItem);

        reportItemModel = (ReportItemModel) getIntent().getSerializableExtra("item");
        category.setText(reportItemModel.getCategory());
        symbol.setText(reportItemModel.getSymbol());
        date.setText(reportItemModel.getDate());
        info.setText(reportItemModel.getInfo());
        insight.setText(reportItemModel.getInsight());
        value.setText("Your result is "+reportItemModel.getValue());

        String color=reportItemModel.getColor();
        if (color.equals("#E64A19")) {
            symbolWithColor.setBackgroundResource(R.drawable.text_design);
            symbolWithColor.setText(reportItemModel.getSymbol());
            symbolWithColor.setTextColor(getApplication().getResources().getColor(R.color.red));
        } else {
            symbolWithColor.setBackgroundResource(R.drawable.text_design2);
            symbolWithColor.setText(reportItemModel.getSymbol());
            symbolWithColor.setTextColor(getApplicationContext().getResources().getColor(R.color.blue));
        }

        infoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ItemActivity.this, PopUpInfo.class);
                intent.putExtra("popUpInfo", reportItemModel.getInfo());
                startActivity(intent);
            }
        });
    }
}